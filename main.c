#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#define STACK_LEN 512


enum op_code {
    op_noop = 0,
    op_push = 1,
    op_pop = 2,
    op_jump = 3,
    op_call = 4,
    op_return = 5,
    op_set = 6,
    op_add = 7,
    op_sub = 8,
    op_mul = 9,
    op_div = 10,
    op_mod = 11,
    op_sifz = 12,
    op_sifnz = 13,
    op_sifeq = 14,
    op_sifneq = 15,
    op_sifgt = 16,
    op_siflt = 17,
    op_out = 18
};

enum op_flag {
    op_arg1_reg = 0b01,
    op_arg2_reg = 0b10,
    op_noargs = 0b10
};

enum _register {
    reg_r1 = 0, // General purpose register
    reg_r2 = 1, // General purpose register
    reg_r3 = 2, // General purpose register
    reg_r4 = 3, // General purpose register
    reg_stk = 4, // Stack index register
    reg_ii = 5 // Instruction index register; read-only
};

struct __attribute__((__packed__)) instruction {
    uint8_t op; // Contains op_code and op_flags
    uint16_t args[2];
};

static inline uint8_t instruction_flags(const struct instruction *instr) {
    return (instr->op & 0b11000000) >> 6;
}

static inline uint8_t instruction_opcode(const struct instruction *instr) {
    return instr->op & ~0b11000000;
}

static inline int instruction_read(int fd, struct instruction *instr, unsigned index) {
    return pread(fd, (void*)instr, sizeof(*instr), index*sizeof(*instr)) - sizeof(*instr);
}


struct runtime {
    uint16_t *stack;
    uint16_t ii;
    uint16_t regs[5];
};

void runtime_init(struct runtime *runtime) {
    runtime->stack = malloc(sizeof(uint16_t)*STACK_LEN);
    runtime->ii = 0;
    runtime->regs[reg_stk] = 0;
}

static inline void runtime_deinit(const struct runtime *runtime) {
    free(runtime->stack);
}

void runtime_debug_dump(const struct runtime *runtime) {
    printf("Register dump:\n"
           " r1=%x r2=%x r3=%x r4=%x stk=%x ii=%x\n",
           runtime->regs[reg_r1], runtime->regs[reg_r2], runtime->regs[reg_r3], runtime->regs[reg_r4], runtime->regs[reg_stk], runtime->ii);
}

static inline void runtime_panic(const struct runtime *runtime, const char *what, const char *description, uint16_t value) {
    fprintf(stderr, "Runtime panic: %s at 0x%x: %s (0x%x)\n", what, runtime->ii, description, value);
    runtime_debug_dump(runtime);
    abort();
}

static inline void runtime_verify_stk(const struct runtime *runtime) {
    if (runtime->regs[reg_stk] > STACK_LEN)
        runtime_panic(runtime, "Stack error", "Stack access out of range", runtime->regs[reg_stk]);
}

uint16_t *runtime_reg(struct runtime *runtime, unsigned index, bool allow_readonly) {
    if (index == reg_ii) {
        if (!allow_readonly)
            runtime_panic(runtime, "Illegal instruction", "Potential write access to instruction index register", index);
        return &runtime->ii;
    }
    if (index > reg_stk)
        runtime_panic(runtime, "Illegal instruction", "Bad register", index);
    return &runtime->regs[index];
}

uint16_t *runtime_arg(struct runtime *runtime, struct instruction* instr, uint8_t op_flags, unsigned index, bool allow_readonly) {
    // Check if value is in register
    int is_register;
    switch (index) {
    case 0: is_register = op_flags & op_arg1_reg; break;
    case 1: is_register = op_flags & op_arg2_reg; break;
    default: runtime_panic(runtime, "Internal error", "Attempted to access bad instruction argument", index);
    }
    // Return constant value if not
    if (!is_register)
        return &instr->args[index];
    // Otherwise return value in register
    return runtime_reg(runtime, instr->args[index], allow_readonly);
}

void runtime_execute(struct runtime *rt, struct instruction *instr) {
    // Get instruction flags
    const uint8_t op_flags = instruction_flags(instr);
    // Execute instruction
    switch (instruction_opcode(instr)) {
    case op_noop: break;
    case op_push: {
        if (!(op_flags & op_noargs)) {
            runtime_verify_stk(rt);
            rt->stack[rt->regs[reg_stk]] = *runtime_arg(rt, instr, op_flags, 0, true);
        }
        ++rt->regs[reg_stk];
    } break;
    case op_pop: {
        --rt->regs[reg_stk];
        if (!(op_flags & op_noargs)) {
            runtime_verify_stk(rt);
            *runtime_arg(rt, instr, op_flags, 0, false) = rt->stack[rt->regs[reg_stk]];
        }
    } break;
    case op_call: {
        runtime_verify_stk(rt);
        rt->stack[rt->regs[reg_stk]] = rt->ii + 1;
        ++rt->regs[reg_stk];
    } // fallthrough
    case op_jump: {
        rt->ii = *runtime_arg(rt, instr, op_flags, 0, true) - 1;
    } break;
    case op_return: {
        --rt->regs[reg_stk];
        runtime_verify_stk(rt);
        rt->ii = rt->stack[rt->regs[reg_stk]] - 1;
    } break;
    case op_set: {
        *runtime_arg(rt, instr, op_flags, 0, false) = *runtime_arg(rt, instr, op_flags, 1, true);
    } break;
    case op_add: {
        *runtime_arg(rt, instr, op_flags, 0, false) += *runtime_arg(rt, instr, op_flags, 1, true);
    } break;
    case op_sub: {
        *runtime_arg(rt, instr, op_flags, 0, false) -= *runtime_arg(rt, instr, op_flags, 1, true);
    } break;
    case op_mul: {
        *runtime_arg(rt, instr, op_flags, 0, false) *= *runtime_arg(rt, instr, op_flags, 1, true);
    } break;
    case op_div: {
        const uint16_t v = *runtime_arg(rt, instr, op_flags, 1, true);
        if (v == 0)
            runtime_panic(rt, "Maths error", "Division by zero", *runtime_arg(rt, instr, op_flags, 0, true));
        *runtime_arg(rt, instr, op_flags, 0, false) /= v;
    } break;
    case op_mod: {
        const uint16_t v = *runtime_arg(rt, instr, op_flags, 1, true);
        if (v == 0)
            runtime_panic(rt, "Maths error", "Modulo by zero", *runtime_arg(rt, instr, op_flags, 0, true));
        *runtime_arg(rt, instr, op_flags, 0, false) %= v;
    } break;
    case op_sifz: {
        if (*runtime_arg(rt, instr, op_flags, 0, true) == 0)
            ++rt->ii;
    } break;
    case op_sifnz: {
        if (*runtime_arg(rt, instr, op_flags, 0, true) != 0)
            ++rt->ii;
    } break;
    case op_sifeq: {
        if (*runtime_arg(rt, instr, op_flags, 0, true) == *runtime_arg(rt, instr, op_flags, 1, true))
            ++rt->ii;
    } break;
    case op_sifneq: {
        if (*runtime_arg(rt, instr, op_flags, 0, true) != *runtime_arg(rt, instr, op_flags, 1, true))
            ++rt->ii;
    } break;
    case op_sifgt: {
        if (*runtime_arg(rt, instr, op_flags, 0, true) > *runtime_arg(rt, instr, op_flags, 1, true))
            ++rt->ii;
    } break;
    case op_siflt: {
        if (*runtime_arg(rt, instr, op_flags, 0, true) < *runtime_arg(rt, instr, op_flags, 1, true))
            ++rt->ii;
    } break;
    case op_out: {
        putchar((char)*runtime_arg(rt, instr, op_flags, 0, true));
    } break;
    default: runtime_panic(rt, "Illegal instruction", "Unknown opcode", instruction_opcode(instr));
    }
    // Increment instruction index
    ++rt->ii;
}


int main(int argc, char **argv) {
    // Check args
    if (argc != 2) {
        printf("Usage: %s <binary>\n", argv[0]);
        return -1;
    }
    // Get args
    const char *binary_path = argv[1];
    // Open binary
    int fd = openat(AT_FDCWD, binary_path, O_RDONLY | O_CLOEXEC);
    if (fd < 0) {
        perror("Failed to open binary");
        return errno;
    }
    // Create and initialize runtime data
    size_t insn_count = 0;
    struct runtime rt;
    runtime_init(&rt);
    // Execute
    struct instruction instr;
    while (instruction_read(fd, &instr, rt.ii) >= 0) {
        ++insn_count;
        runtime_execute(&rt, &instr);
    }
    // Dump runtime data
    runtime_debug_dump(&rt);
    // Clean up
    runtime_deinit(&rt);

    printf("Finished after %zu instructions\n", insn_count);
}
