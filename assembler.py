# This Python file uses the following encoding: utf-8

from sys import argv, stderr, exit
from json import load as load_json
from struct import pack

# Enums
op_codes = {
    "NOOP": 0,
    "PUSH": 1,
    "POP": 2,
    "JUMP": 3,
    "CALL": 4,
    "RETURN": 5,
    "SET": 6,
    "ADD": 7,
    "SUB": 8,
    "MUL": 9,
    "DIV": 10,
    "MOD": 11,
    "SIFZ": 12,
    "SIFNZ": 13,
    "SIFEQ": 14,
    "SIFNEQ": 15,
    "SIFGT": 16,
    "SIFLT": 17,
    "OUT": 18
}
op_flags = {
    "arg1_reg": 0b01,
    "arg2_reg": 0b10,
    "noargs": 0b10
}
regs = {
    "r1": 0, # General purpose register
    "r2": 1, # General purpose register
    "r3": 2, # General purpose register
    "r4": 3, # General purpose register
    "stk": 4, # Stack index register
    "ii": 5 # Instruction index register; read-only
}

# Check args
if len(argv) != 3:
    print(f"Usage: {argv[0]} <source> <binary output>", file=stderr)
    exit(-1)

# Get args
source_path = argv[1]
output_path = argv[2]

# Open file
source_file = open(source_path, "r")
output_file = open(output_path, "wb")

# Parse JSON
source = load_json(source_file)
source_file.close()

# Generate output
output = []
for instr in source:
    instr_args = instr["args"]

    op_code = op_codes[instr["name"]]
    op_flagss = 0

    args = [0, 0]
    arg_count = len(instr_args)
    if arg_count == 0:
        op_flagss |= op_flags["noargs"];
    else:
        for it in range(arg_count):
            if type(instr_args[it]) == str:
                args[it] = regs[instr_args[it]]
                op_flagss |= op_flags[f"arg{it+1}_reg"];
            else:
                args[it] = instr_args[it]

    op = op_code | (op_flagss << 6)
    output.append({"op": op, "args": args})

# Print intermediate output
print(output)

# Write to output file
for instr in output:
    output_file.write(pack("<B", instr["op"]))
    args = instr["args"]
    output_file.write(pack("<H", args[0]))
    output_file.write(pack("<H", args[1]))
