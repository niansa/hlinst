cmake_minimum_required(VERSION 3.5)

project(hlinst LANGUAGES C)

add_executable(hlinst main.c)

install(TARGETS hlinst
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
)
